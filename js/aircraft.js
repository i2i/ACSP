"use strict";

class Aircraft {
  constructor(type, size, count) {
    this._type = type;
    this._size = size;
    this._count = count;
    Aircraft.count++;
  }

  get uid() {
    return this._count;
  }

  get type() {
    return this._type;
  }

  get size() {
    return this._size;
  }

  get priority() {
    // Binary ranking of plane types such that Passenger Large = 0, Passenger Small = 1,
    // Cargo Large = 2, and Cargo Small = 3. Planes with lower priority ranking get precendence
    // in dequeuing. When two planes have the same priority ranking, the plane with the lower
    // index in the queue gets precendence in dequeuing.
    return (
      0 + (this._type == "Cargo" ? 2 : 0) + (this._size == "Small" ? 1 : 0)
    );
  }
}

// Initialize count
Aircraft.count = 1;

export { Aircraft as default };
