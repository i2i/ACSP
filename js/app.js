"use strict";

import queueDiv from "./queueContent.js";
import queue from "./queue.js";
import Aircraft from "./aircraft.js";

const buttonBoot = document.querySelector("#boot");
const buttonEnqRand = document.querySelectorAll("#enqueue-random");
const buttonDequeue = document.querySelector("#dequeue");

function systemOn() {
  Object.values(buttonEnqRand).map((button) => {
    button.disabled = false;
  });
  buttonDequeue.disabled = false;
  buttonBoot.disabled = true;
}

function createRandomAc(num) {
  for (let i = 0; i < num; i++) {
    const randType =
      Math.floor(Math.random() * 2) + 1 == 1 ? "Cargo" : "Passenger";
    const randSize = Math.floor(Math.random() * 2) + 1 == 1 ? "Large" : "Small";
    let randAc = new Aircraft(randType, randSize, Aircraft.count);
    queue.enqueue(randAc);
  }
}

buttonBoot.addEventListener("click", (event) => {
  event.stopPropagation;
  systemOn();
  queueDiv();
});

Object.values(buttonEnqRand).map((button) => {
  button.addEventListener("click", (event) => {
    event.stopPropagation;
    const count = button.getAttribute("data-count");
    createRandomAc(count);
    alert(`Created ${count} plane${count > 1 ? "s" : ""}.`);
    queueDiv();
  });
});

buttonDequeue.addEventListener("click", (event) => {
  event.stopPropagation;
  const dequeuedAc = queue.dequeue();
  alert(
    `Dequeued Aircraft: ${dequeuedAc.uid} - ${dequeuedAc.type} | ${dequeuedAc.size}`
  );
  queueDiv();
});
