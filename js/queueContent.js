"use strict";

import queue from "../js/queue.js";

export default function queueDiv() {
  const queueCounter = document.querySelector("#queue-counter");
  const queueDiv = document.querySelector("#content-queue");
  const buttonDequeue = document.querySelector("#dequeue");
  let queueContent = "";
  queue.length > 0
    ? queue.list.map((plane) => {
        const icon = plane.type == "Passenger" ? "user" : "cube";
        queueContent += `
        <div class="plane">
          <div class="plane-uid">
            <h1>${plane.uid}</h1>
          </div>
          <div class="plane-description">
            <p><strong>Size:</strong> ${plane.size}<br>\n
            <strong>Type:</strong> ${plane.type}<br>\n
            <strong>Priority:</strong> ${plane.priority}</p>\n
          </div>
          <div class="plane-icon">
            <i class="fa fa-${icon}"></i>
          </div>
        </div>`;
      })
    : (queueContent = "<p>There are no planes queued.</p>");
  buttonDequeue.disabled = queue.length === 0;
  queueDiv.innerHTML = queueContent;
  queueCounter.innerHTML = `(${queue.length})`;
}
