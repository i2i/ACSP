"use strict";

class PriorityQueue {
  constructor() {
    this.values = [];
  }

  get length() {
    return this.values.length;
  }

  get list() {
    let value = [];
    for (let i = 0; i < this.length; i++) {
      value.push(this.values[i]);
    }
    return value.sort((a, b) => a.uid - b.uid);
  }

  swap(index1, index2) {
    let temp = this.values[index1];
    this.values[index1] = this.values[index2];
    this.values[index2] = temp;
    return this.values;
  }

  bubbleUp() {
    let index = this.values.length - 1;
    while (index > 0) {
      let parentIndex = Math.floor((index - 1) / 2);
      if (this.values[parentIndex].priority > this.values[index].priority) {
        this.swap(index, parentIndex);
        index = parentIndex;
      } else {
        break;
      }
    }
    return 0;
  }
  enqueue(value) {
    this.values.push(value);
    this.bubbleUp();
    return this.values;
  }

  bubbleDown() {
    let parentIndex = 0;
    const length = this.values.length;
    const elementPriority = this.values[0].priority;
    const elementUid = this.values[0].uid; //CS
    while (true) {
      let leftChildIndex = 2 * parentIndex + 1;
      let rightChildIndex = 2 * parentIndex + 2;
      let leftChildPriority, rightChildPriority, leftChildUid, rightChildUid;
      let indexToSwap = null;
      if (leftChildIndex < length) {
        leftChildPriority = this.values[leftChildIndex].priority;
        leftChildUid = this.values[leftChildIndex].uid;
        if (
          leftChildPriority < elementPriority ||
          (leftChildPriority == elementPriority && leftChildUid < elementUid)
        ) {
          indexToSwap = leftChildIndex;
        }
      }
      if (rightChildIndex < length) {
        rightChildPriority = this.values[rightChildIndex].priority;
        rightChildUid = this.values[rightChildIndex].uid;

        if (rightChildPriority < elementPriority && indexToSwap === null) {
          indexToSwap = rightChildIndex;
        } else if (
          rightChildPriority < leftChildPriority &&
          indexToSwap !== null
        ) {
          this.swap(rightChildIndex, leftChildIndex);
          rightChildIndex = leftChildIndex;
          indexToSwap = rightChildIndex;
        }
      }
      if (indexToSwap === null) {
        break;
      }
      this.swap(parentIndex, indexToSwap);
      parentIndex = indexToSwap;
    }
  }

  dequeue() {
    let shifted = this.values.shift();
    if (this.values.length > 1) {
      this.bubbleDown();
    }

    return shifted;
  }
}

const queue = new PriorityQueue();

export { queue as default };
