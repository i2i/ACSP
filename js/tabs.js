"use strict";

let tab = {
  nav: null,
  cont: null,
  init: function () {
    tab.nav = document
      .getElementById("tab-nav")
      .getElementsByClassName("tabnav");
    tab.cont = document
      .getElementById("tab-contents")
      .getElementsByClassName("tabcont");

    if (
      tab.nav.length == 0 ||
      tab.cont.length == 0 ||
      tab.nav.length !== tab.cont.length
    ) {
      console.log("Check number of tabs and content sections.");
    } else {
      for (let i = 0; i < tab.nav.length; i++) {
        tab.nav[i].dataset.pos = i;
        tab.nav[i].addEventListener("click", tab.switch);
      }

      tab.nav[0].classList.add("active");
      tab.cont[0].classList.add("active");
    }
  },

  switch: function () {
    for (let t of tab.nav) {
      t.classList.remove("active");
    }
    for (let t of tab.cont) {
      t.classList.remove("active");
    }

    tab.nav[this.dataset.pos].classList.add("active");
    tab.cont[this.dataset.pos].classList.add("active");
  }
};

window.addEventListener("load", tab.init);
